
class Transition:
    def __init__(self, start_state, symbol, end_state):
        self.start_state = start_state
        self.symbol = symbol
        self.end_state = end_state
    

class Automaton:
    def __init__(self):
        self.alphabet = []
        self.states = []
        self.initial_states = []
        self.final_states = []
        self.transitions = []
        self.epsilon = "ep"
        
    def set_alphabet(self, alphabet):
        self.alphabet = alphabet
        
    def set_states(self, states):
        self.states = states
    
    def add_initial_state(self, initial_states):
        self.initial_states = initial_states

    def add_final_state(self, final_states):
        self.final_states = final_states
    
        
    def add_transition(self, transition): # start_state, symbol, end_state
        self.transitions.append(transition)
    
    def print_automaton(self):
        transitions = []
        for t in self.transitions:
            transitions.append("("+t.start_state+", "+t.symbol+", "+t.end_state+")")
        print(
            "\n ============ AUTOMATA CRÉE ========== \n"
            "> ALPHABET E : { "+ ", ".join(self.alphabet) + " }\n"
            "> ETATS Q : { "+ ", ".join(self.states) + " }\n"
            "> ETATS INITIAUX qI : { "+ ", ".join(self.initial_states) + " }\n"
            "> ETATS FINAUX qF : { "+ ", ".join(self.final_states) + " }\n"
            "> TRANSITIONS \n T : { "+ ", ".join(transitions) + " }\n"
        )


