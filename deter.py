def epsilon_closure(states, transitions):
    closure = set(states)
    queue = list(states)

    while queue:
        current_state = queue.pop()
        next_states = transitions.get((current_state, ''), [])
        for state in next_states:
            if state not in closure:
                closure.add(state)
                queue.append(state)

    return closure

def move(states, transitions, symbol):
    move_states = set()
    for state in states:
        next_states = transitions.get((state, symbol), [])
        move_states.update(next_states)
    return move_states

def determinize(afn_states, afn_transitions, afn_start, afn_finals, alphabet):
    dfa_states = []
    dfa_transitions = {}
    dfa_start = epsilon_closure(afn_start, afn_transitions)
    dfa_finals = []
    unmarked = [dfa_start]

    while unmarked:
        current_states = unmarked.pop()
        dfa_states.append(current_states)

        for symbol in alphabet:
            next_states = move(current_states, afn_transitions, symbol)
            epsilon_states = epsilon_closure(next_states, afn_transitions)
            if epsilon_states:
                dfa_transitions[(tuple(current_states), symbol)] = epsilon_states
                if epsilon_states not in dfa_states:
                    unmarked.append(epsilon_states)

        if any(state in afn_finals for state in current_states):
            dfa_finals.append(current_states)

    return dfa_states, dfa_transitions, dfa_start, dfa_finals

# Exemple d'utilisation
afn_states = {'A', 'B', 'C'}
afn_transitions = {
    ('A', '0'): ['B'],
    ('B', '1'): ['C'],
    ('C', '0'): ['A', 'C'],
    ('C', '1'): ['B']
}
afn_start = ['A']
afn_finals = ['C']
alphabet = ['0', '1']

dfa_states, dfa_transitions, dfa_start, dfa_finals = determinize(afn_states, afn_transitions, afn_start, afn_finals, alphabet)

print("États de l'automate déterminisé :", dfa_states)
print("Transitions de l'automate déterminisé :", dfa_transitions)
print("État initial de l'automate déterminisé :", dfa_start)
print("États finaux de l'automate déterminisé :", dfa_finals)
