from models import Automaton
from utils import (
    init_alphabet, init_states, init_initial_states, 
    init_final_states, init_transitions, determine_automaton_type
)

def creat_automaton():
    # Créer et afficher un automate

    auto = Automaton()

    init_alphabet(auto=auto)
    init_states(auto=auto)
    init_initial_states(auto=auto)
    init_final_states(auto=auto)
    init_transitions(auto=auto)
    auto.print_automaton()

    # Déterminer le type d'automate

    auto_type = determine_automaton_type(auto=auto)
    print("Type d'automate :", auto_type)
