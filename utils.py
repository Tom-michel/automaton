from models import Transition

def init_alphabet(auto):
    alphabet = input(
        "\nINITIALISATION DE L'ALPHABET\n"
        "\t Epsilon est représenté par 'ep'\n"
        "\t Saisir les symboles séparés par des espaces\n"
        "\t Format : <symbole> <symbole> <symbole> ... Exp: a b c\n"
        "1. Alphabet : "
    ).split()
    
    def is_valid_alphabet():
        for symbol in alphabet:
            if alphabet.count(symbol) > 1:
                print("Error: Alphabet not added\n>> symbol ["+symbol+"] is already in alphabet")
                return False
            elif symbol == "ep":
                print("Error: Alphabet not added\n>> symbol ["+symbol+"] (epsilon) not allowed to add in alphabet")
                return False
            else:
                return True
    
    if is_valid_alphabet():
        auto.set_alphabet(alphabet)
    else:
        init_alphabet(auto)

def init_states(auto):
    states = input(
        "\nINITIALISATION DES ETATS\n"
        "\t Saisir les etats séparés par des espaces\n"
        "\t Format : <etat> <etat> <etat> ... Exp: 0 1 2\n"
        "2. Ensemble d'états : "
    ).split()

    def is_valid_statelist():
        for state in states:
            if states.count(state) > 1:
                print("Error: State not added\n>> state ["+state+"] is already in states list")
                return False
            else:
                return True
    
    if is_valid_statelist():
        auto.set_states(states)
    else:
        init_states(auto)

def init_initial_states(auto):
    initial_states = input(
        "\nINITIALISATON DE(S) ETAT(S) INITIAUX\n"
        "3. États initiaux (séparés par des espaces) : "
    ).split()

    def is_valid_state():
        for state in initial_states:
            if state not in auto.states:
                print("Error: State not added\n>> state ["+state+"] is not in state list")
                return False
            elif initial_states.count(state) > 1:
                print("Error: State not added\n>> state ["+state+"] is already in list")
                return False
            else:
                return True
    
    if is_valid_state():
        auto.add_initial_state(initial_states)
    else:
        init_initial_states(auto)

def init_final_states(auto):
    final_states = input(
        "\nINITIALISATON DE(S) ETAT(S) FINAUX\n"
        "4. États finaux (séparés par des espaces) : "
    ).split()

    def is_valid_state():
        for state in final_states:
            if state not in auto.states:
                print("Error: State not added\n>> state ["+state+"] is not in state list")
                return False
            elif final_states.count(state) > 1:
                print("Error: State not added\n>> state ["+state+"] is already in list")
                return False
            else:
                return True
    
    if is_valid_state():
        auto.add_final_state(final_states)
    else:
        init_final_states(auto)

def init_transitions(auto):
    print(
        "\nINITIALISATION DES TRANSITIONS\n"
        "\tFormat : <qi> <symbole>ou<epsilon> <qf>. Exp: 0 a 1 ou 0 ep 1\n"
        "\tTapez 'q' pour quitter\n"
    )
    while True:
        transition = input("Transition : ")
        if transition == "q":
            break
        if len(transition.split()) != 3:
            print("Error: Transition not added\n>> Type error or operands missed")
            init_transitions(auto)
        start_state, symbol, end_state = transition.split()
        new_transition = Transition(start_state, symbol, end_state)
        
        def is_valid_transition():
            if new_transition.start_state not in auto.states:
                print("Error: Transition not added\n>> start state ["+new_transition.start_state+"] is not in states list")
                return False
            elif new_transition.symbol not in auto.alphabet and new_transition.symbol != "ep" :
                print("Error: Transition not added\n>> symbol ["+new_transition.symbol+"] is not in alphabet")
                return False
            elif new_transition.end_state not in auto.states:
                print("Error: Transition not added\n>> end state ["+new_transition.end_state+"] is not in states list")
                return False
            else:
                return True
            
        
        if is_valid_transition():
            auto.add_transition(new_transition)
        else:
            init_transitions(auto)

# get the auto type
def determine_automaton_type(auto):
    # Vérifier les ε-transitions
    for transition in auto.transitions:
        if transition.symbol == auto.epsilon:
            return "'ε-AFN', Car il contient des ε-transitions"

    # Vérifier les états initiaux
    if len(auto.initial_states) > 1:
        return "AFN, Car il contient plusieurs états initiaux"

    # Verifier si il contient plusieurs transitions sortantes d'un etat pour le même caratère
    states_with_multiple_transitions = []
    for state in auto.states:
        outgoing_symbols = set()
        for transition in auto.transitions:
            if transition.start_state == state:
                if transition.symbol in outgoing_symbols:
                    states_with_multiple_transitions.append(state)
                    break
                else:
                    outgoing_symbols.add(transition.symbol)
    if len(states_with_multiple_transitions) > 0:
        return "AFN, Car il contient plusieurs transitions sortantes d'un etat pour le même caratère"
    
    # # Vérifier les transitions pour chaque état et symbole de l'alphabet
    # for state in auto.states:
    #     for symbol in auto.alphabet:
    #         transitions = [
    #             t for t in auto.transitions
    #             if t.start_state == state and t.symbol == symbol
    #         ]
    #         if len(transitions) != 1:
    #             return "AFN, Car il contient des transitions manquantes"
    
    # Si aucune condition n'est remplie, l'automate est un AFD
    return "AFD, Car il ne contient aucune ε-transition, ni plusieurs états initiaux, ni des transitions manquantes"
