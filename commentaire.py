import re

def detect_comments(text):
    pattern = r'/%\*.*?\*/|/\*.*?\*/'
    comments = re.findall(pattern, text, re.DOTALL)
    return comments

# Exemple d'utilisation
text = '''
Ce texte contient des commentaires.

/* Ceci est un commentaire. */

Ce texte ne contient pas de commentaire.

/* 
   Ceci est un commentaire multi-ligne.
   /* Ceci n'est pas un commentaire imbriqué. */
*/

Voici un autre commentaire : /%* Ceci est un commentaire avec un astérisque suivi de la fin de commentaire *%/

Fin du texte.
'''

comment_list = detect_comments(text)

for comment in comment_list:
    print(comment)
