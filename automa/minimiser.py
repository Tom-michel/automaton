from automa.automat import Automat


def entrer_automate():
    alphabet = input("Entrez l'alphabet de l'automate (séparés par des virgules) : ").split(",")
    etats = input("Entrez les états de l'automate (séparés par des virgules) : ").split(",")
    etat_initial = input("Entrez l'état initial de l'automate : ")
    etats_finaux = input("Entrez les états finaux de l'automate (séparés par des virgules) : ").split(",")

    fonction_transition = {}
    for etat in etats:
        for symbole in alphabet:
            transition = input(f"Entrez la transition pour l'état {etat} avec le symbole {symbole} : ")
            fonction_transition[(etat, symbole)] = transition
    print(fonction_transition)
    automate = Automat(alphabet, etats, etat_initial, etats_finaux, fonction_transition)
    return automate


def determiniser_automate(automate: Automat):
    if automate.is_afd():
        return automate

    etats_deterministes = set()
    etats_non_deterministes = {automate.initial_state}
    fonction_transition_deterministe = {}

    while len(etats_non_deterministes) > 0:
        etat_non_deterministe = etats_non_deterministes.pop()
        etats_deterministes.add(etat_non_deterministe)

        for symbole in automate.alphabet:
            etats_suivants = set()

            for etat_non_deterministe in etats_deterministes:
                etat_suivant = automate.transition_function.get((etat_non_deterministe, symbole))
                if etat_suivant is not None:
                    etats_suivants.add(etat_suivant)

            etat_deterministe = ",".join(sorted(list(etats_suivants)))
            fonction_transition_deterministe[(etat_non_deterministe, symbole)] = etat_deterministe

            if etat_deterministe not in etats_deterministes:
                etats_non_deterministes.add(etat_deterministe)

    etats_finaux_deterministes = set()
    for etat_deterministe in etats_deterministes:
        for etat_final in automate.final_state:
            if etat_final in etat_deterministe:
                etats_finaux_deterministes.add(etat_deterministe)
                break

    automate_deterministe = Automat(
                                automate.alphabet,
                                sorted(list(etats_deterministes)),
                                automate.initial_state,
                                sorted(list(etats_finaux_deterministes)),
                                fonction_transition_deterministe)
    return automate_deterministe


def minimiser_automate(automate: Automat):
    automate_deterministe = determiniser_automate(automate)

    partition = [automate_deterministe.final_state,
                 list(set(automate_deterministe.states) - set(automate_deterministe.final_state))]
    nouvelle_partition = []

    while partition != nouvelle_partition:
        partition = nouvelle_partition.copy()
        nouvelle_partition = []

        for groupe in partition:
            if len(groupe) > 1:
                sous_partitions = []
                etats_traites = set()

                for etat in groupe:
                    if etat not in etats_traites:
                        sous_partition = [etat]
                        etats_traites.add(etat)

                        for autre_etat in groupe:
                            if autre_etat not in etats_traites and ont_meme_transition(automate_deterministe, etat,
                                                                                       autre_etat, partition):
                                sous_partition.append(autre_etat)
                                etats_traites.add(autre_etat)

                        sous_partitions.append(sous_partition)

                nouvelle_partition.extend(sous_partitions)
            else:
                nouvelle_partition.append(groupe)

    etats_minimaux = []
    fonction_transition_minimale = {}

    for groupe in nouvelle_partition:
        etat_minimal = groupe[0]
        etats_minimaux.append(etat_minimal)

        for symbole in automate_deterministe.alphabet:
            etat_suivant = automate_deterministe.transition_function.get((etat_minimal, symbole))

            for autre_groupe in nouvelle_partition:
                if etat_suivant in autre_groupe:
                    fonction_transition_minimale[(etat_minimal, symbole)] = autre_groupe[0]
                    break

    automate_minimal = Automat(automate.alphabet, sorted(etats_minimaux), automate_deterministe.initial_state,
                                automate_deterministe.final_state, fonction_transition_minimale)
    return automate_minimal


def ont_meme_transition(automate: Automat, etat1, etat2, partition):
    for symbole in automate.alphabet:
        etat_suivant1 = automate.transition_function.get((etat1, symbole))
        etat_suivant2 = automate.transition_function.get((etat2, symbole))

        if etat_suivant1 is None and etat_suivant2 is not None or etat_suivant1 is not None and etat_suivant2 is None:
            return False

        if etat_suivant1 is not None and etat_suivant2 is not None:
            groupe_etat_suivant1 = get_groupe_contenant_etat(etat_suivant1, partition)
            groupe_etat_suivant2 = get_groupe_contenant_etat(etat_suivant2, partition)

            if groupe_etat_suivant1 != groupe_etat_suivant2:
                return False

    return True


def get_groupe_contenant_etat(etat, partition):
    for groupe in partition:
        if etat in groupe:
            return groupe


